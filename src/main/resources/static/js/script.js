$(".search_bar_input").focus(function () {
    $(".search_bar").css({"background-color": "#249170"});
});
$(".search_bar_input").focusout(function () {
    $(".search_bar").css({"background-color": "#0D7C5A"});
});
$(".username_input").focus(function () {
    $(".username_panel").css({"background-color": "#249170"});
});
$(".password_input").focus(function () {
    $(".password_panel").css({"background-color": "#249170"});
});
$(".username_input").focusout(function () {
    $(".username_panel").css({"background-color": "transparent"});
});
$(".password_input").focusout(function () {
    $(".password_panel").css({"background-color": "transparent"});
});
$(".login_button").on("click", function login() {
    window.location.replace("index.html");

});
$(".nav_bar_toggle").on("click", function login() {
    $(".sub_main").toggleClass("sub_main_toggled");
    $(".main").toggleClass("main_toggled");
    $(".nav_bar").toggle();
});

