$( document ).ready(function() {
$("#save_add_user_button").on("click", function addUser() {
    var jsonData = {
        client_id: $("#add_client_id").val(),
        client_secret: $("#add_client_secret").val(),
        authorities: $('.add_authorities:checked').map(function() {
            return this.value;
        }).get().join(),
        access_token_validity: $("#add_access_token_validity").val(),
        refresh_token_validity: $("#add_refresh_token_validity").val()
    };
    $.ajax({
        type: "post",
        url: "/oauth/add",
        data: JSON.stringify(jsonData),
        contentType: "application/json",
        processData: false,
        success: function (response) {
            alert("Response: " + response);
        },
        error: function (errorThrown) {
            alert("Error: " + errorThrown);
        }
    })
});
});
