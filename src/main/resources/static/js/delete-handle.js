$( document ).ready(function() {
    $("#delete_user_button").on("click", function deleteUser() {
        console.log("delete");

        var jsonData= {
            client_id: delete_id
        };

        $.ajax({
            type: "DELETE",
            url: "/oauth/delete",
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            processData: false,
            success: function (response) {
                alert("Response: " + response);
                console.log(response);
            },
            error: function (errorThrown) {
                alert("Error: " + errorThrown);
            }
        })
    });
});