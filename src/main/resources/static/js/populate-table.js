var delete_id;
function deleteId(id) {
    delete_id = id;
}

$(document).ready(function () {
    $.getJSON('/oauth/all', function (data) {
        var table = '<table><thead><tr><th>User</th><th>Authorities</th><th>Access Token Validity</th><th>Refresh Token Validity</th><th>Action</th></tr></thead><tbody>';

        $.each(data, function(user_num, item) {
            table += '<tr>';
            table += '<td>' + " " + item.client_id + '</td>';
            table += '<td>' +  item.authorities + '</td>';
            table += '<td>' +  item.access_token_validity+ '</td>';
            table += '<td>' +  item.refresh_token_validity+ '</td>';
            table += '<td class="nowrap"><button id="edit"data-toggle="modal" data-target="#edit_user_modal">edit</button><button id= "delete" onclick="deleteId(\''+item.client_id+'\')" data-toggle="modal" data-target="#delete_user_modal">delete</button></td>';
            table += '</tr>';
        });
        table += '</tbody></table>';
        $(".table_container").html(table);


    });
});