$(document).ready(function () {
    $("#save_edit_user_button").click(function () {
        var jsonEditData = {
            client_id: $("#edit_user_id").val(),
            client_secret: $("#edit_user_secret").val(),
            authorities: $('.edit_authorities:checked').map(function () {
                return this.value;
            }).get().join(),
            access_token_validity:
                $("#edit_access_token_validity").val(),
            refresh_token_validity:
                $("#edit_refresh_token_validity").val()
        };

        $.ajax({
            type: "post",
            url: "/oauth/update",
            data: JSON.stringify(jsonEditData),
            contentType: "application/json",
            processData: false,
            success: function (response) {
                alert("Response: " + response);
                console.log(response);
            },
            error: function (errorThrown) {
                alert("Error: " + errorThrown);
            }
        })
    });
});