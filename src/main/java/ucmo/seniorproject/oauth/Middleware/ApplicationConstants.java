package ucmo.seniorproject.oauth.Middleware;

public class ApplicationConstants {
    public static final String CLIENT_ID = "user_id";
    public static final String CLIENT_SECRET = "password";
    public static final String RESOURCE_ID = "resource_id";
    public static final String AUTHENTICATION_SERVER_URL = "authentication_server_url";
    public static final String CONFIG_FILE_PATH = "http://www.example.com";
    public static final String RESOURCE_SERVER_URL = "resource_server_url";
    public static final String WEB_SERVER_REDIRECT = "http://www.example.com";
    public static final String AUTHORIZED_GRANT_TYPE = "read,write,delete";
    public static final String AUTHORIZATION = "Authorization";
    public static final String AUTHORITIES = "role_client role_trusted_client";
    public static final String ADDITIONAL_INFORMATION = "additional_information";
    public static final String BASIC = "Basic";
    public static final String JSON_CONTENT = "application/json";
    public static final String XML_CONTENT = "application/xml";
    public static final String URL_ENCODED_CONTENT = "application/x-www-form-urlencoded";


    public static final int ACCESS_TOKEN_VALIDITY = 30;
    public static final int REFRESH_TOKEN_VALIDITY = 30;
    public static final int HTTP_OK = 200;
    public static final int HTTP_FORBIDDEN = 403;
    public static final int HTTP_UNAUTHORIZED = 401;


}
