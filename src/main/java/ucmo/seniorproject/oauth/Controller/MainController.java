package ucmo.seniorproject.oauth.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ucmo.seniorproject.oauth.Entity.User;
import ucmo.seniorproject.oauth.Middleware.MiddlewarePasswordEncoder;
import ucmo.seniorproject.oauth.Middleware.ApplicationConstants;
import ucmo.seniorproject.oauth.Repository.UserRepository;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


@Controller    // This means that this class is a Controller
@RequestMapping(path = "/oauth") // This means URL's start with /demo (after Application path)
public class MainController {
    @Autowired // This means to get the bean called userRepository
    private UserRepository userRepository;

    @Autowired
    private MiddlewarePasswordEncoder passwordEncoder;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<String> addNewUser(@RequestBody User user) throws Exception {
        try {

            if (userRepository.findOne(user.getClient_id()) != null) {
                return new ResponseEntity<>("User " + user.getClient_id() + "already exists.", HttpStatus.BAD_REQUEST);
            }

            passwordEncoder.afterPropertiesSet();

            user.setClient_secret(passwordEncoder.encode(user.getClient_secret()));

            if (user.getAccess_token_validity() == null) {
                user.setAccess_token_validity(ApplicationConstants.ACCESS_TOKEN_VALIDITY);
            }

            if (user.getRefresh_token_validity() == null) {
                user.setRefresh_token_validity(ApplicationConstants.REFRESH_TOKEN_VALIDITY);
            }
            user.setWeb_server_redirect_uri(ApplicationConstants.WEB_SERVER_REDIRECT);
            user.setAuthorized_grant_types(ApplicationConstants.AUTHORIZED_GRANT_TYPE);
            userRepository.save(user);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Error while creating new user.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("User " + user.getClient_id() + " created.", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<String> updateUser(@RequestBody User user) throws Exception {
        try {
            User oldUser = userRepository.findOne(user.getClient_id());

            if (user.getClient_secret().equals("")) {
                user.setClient_secret(oldUser.getClient_secret());
            }
            else {
                passwordEncoder.afterPropertiesSet();
                user.setClient_secret(passwordEncoder.encode(user.getClient_secret()));
            }

            if (user.getAccess_token_validity() == null) {
                user.setAccess_token_validity(oldUser.getAccess_token_validity());
            }

            if (user.getRefresh_token_validity() == null) {
                user.setRefresh_token_validity(oldUser.getRefresh_token_validity());
            }

            userRepository.save(user);
        } catch (Exception e) {
            return new ResponseEntity<>("Error while updating user", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>("User " + user.getClient_id() + " updated.", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public @ResponseBody String delete(@RequestBody User user) {
        userRepository.delete(user.getClient_id());
        return "successful";
    }

    private String responseCodes() throws IOException {
        URL url = new URL("http://localhost:8080");
        HttpURLConnection URLConnection = (HttpURLConnection) url.openConnection();
        URLConnection.setRequestMethod("POST");
        URLConnection.connect();

        int httpStatusCode = URLConnection.getResponseCode();
        if(httpStatusCode == 200){
            return "OK";
        } else if(httpStatusCode == 202){
            return "ACCEPTED";
        } else if(httpStatusCode == 403){
            return "FORBIDDEN";
        } else if(httpStatusCode == 401){
            return "UNAUTHORIZED";
        }
        return "DONE";
    }

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<User> getAllUsers() {
        // This returns a JSON or XML with the users
        return userRepository.findAll();
    }
}
