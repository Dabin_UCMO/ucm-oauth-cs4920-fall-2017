package ucmo.seniorproject.oauth.Repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ucmo.seniorproject.oauth.Entity.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User,String>{

}